package com.galaxy;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import model.Address;

import model.EmployeDao;
import model.Employee;
import model.User;

@Controller
public class ControllerDemo 
{

	private ApplicationContext conn;  //declaring applicationContext.xml file
	
	@RequestMapping("/home")          //go to home page
	public String view1()
	{
		return "home";
		
	}
	@RequestMapping("/register")      //go to register page
	public String view2(Model m, Model m1)
	{
		conn = new ClassPathXmlApplicationContext("ApplicationContext.xml");
		Employee emp =conn.getBean("info",Employee.class);
		m.addAttribute("bean",emp);
		
		//Address addr =conn.getBean("info1",Address.class);
		//m1.addAttribute("bean",addr);
		
		return "register";
		
	}
	
	@RequestMapping("/store")
	public String view3(@ModelAttribute("bean") Employee e,Address a1 ,Model m )
	{
		conn = new ClassPathXmlApplicationContext("ApplicationContext.xml");
		EmployeDao obj = conn.getBean("dao",EmployeDao.class);
		//obj.saveData(e);
		obj.saveData(e,a1);
		
		//AddressDao obj1 = conn.getBean("dao1",AddressDao.class);
		//obj1.saveData(a);
		m.addAttribute("msg","Record inserted successfully");
		
		return "register";
		 
	}
	
	
	@RequestMapping("/login")
	public String view4(Model m)
	{
		conn = new ClassPathXmlApplicationContext("ApplicationContext.xml");
		Employee obj = conn.getBean("info",Employee.class);
		m.addAttribute("bean",obj);
		return "login";
		
	}
	
	@RequestMapping("/sign")
	public String login(@ModelAttribute("bean") Employee emp,HttpServletRequest request)
	{
		conn = new ClassPathXmlApplicationContext("ApplicationContext.xml");
		EmployeDao obj = conn.getBean("dao",EmployeDao.class);
		boolean flag =obj.login(emp);
		if(flag)
		{
			User usr = conn.getBean("ur",User.class);
			usr.setEmail(emp.getEmail());
			usr.setPassword(emp.getPassword());
			usr.setFlag(1);
			HttpSession session = request.getSession();
			session.setAttribute("data", usr);			
			return "afterlogin";
		}
		else
		{
			//return "redirect:index.jsp";
			
			return "home";
		}
	}
	
	
	@RequestMapping("/userlist")          //go to home page
	public String view5(Model m)
	{
		conn = new ClassPathXmlApplicationContext("ApplicationContext.xml");
		EmployeDao obj = conn.getBean("dao",EmployeDao.class);
		//Address a =conn.getBean("info1",Address.class);
		List list = obj.displayData();
		if(!list.isEmpty())
		{
		m.addAttribute("data",list);
		}
		else
		{
			m.addAttribute("msg","Data not found");
		}
		return  "userlist";
		
	}
	
	@RequestMapping("/find")
	public String view6(Model m)
	{
		conn = new ClassPathXmlApplicationContext("ApplicationContext.xml");
		Employee emp =conn.getBean("info",Employee.class);
		m.addAttribute("bean",emp);
		
		return "search";        //goes to search page
		
	}
	
	@RequestMapping("/search")       
	public String view6(@ModelAttribute("bean")Employee e,Model m)
	{
		conn = new ClassPathXmlApplicationContext("ApplicationContext.xml");
		EmployeDao obj = conn.getBean("dao",EmployeDao.class);
		List list = obj.searchData(e);
		if(!list.isEmpty())
		{
		m.addAttribute("li",list);
		}
		else
		{
			m.addAttribute("msg","Data not found");
		}
		return "search";
		
	}
	
	@RequestMapping("/update")
	public String view7(@ModelAttribute("bean") Employee emp ,Model m)
	{
		conn = new ClassPathXmlApplicationContext("ApplicationContext.xml");
		EmployeDao obj = conn.getBean("dao",EmployeDao.class);
		obj.updateData(emp);
		m.addAttribute("msg", "Record updated successfully..");
		return "search";
	}
	
	@RequestMapping("/delete")
	public String view8(@ModelAttribute("bean") Employee emp ,Model m)
	{
		conn = new ClassPathXmlApplicationContext("ApplicationContext.xml");
		EmployeDao obj = conn.getBean("dao",EmployeDao.class);
		obj.deleteData(emp);
		m.addAttribute("msg", "Record deleted successfully..");
		return "search";
		
	}
	
}
