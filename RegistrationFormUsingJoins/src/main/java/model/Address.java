package model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="Address1")
public class Address 
{
  
  @Id
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  @Column(name="addr_id")
  private int addr_id;
  
  @Column(name="City")
  private String city;
  
  @OneToOne(mappedBy = "address")
  private Employee employee;

	
	
	public int getAddr_id()
	{
		return addr_id;
	}
	
	public void setAddr_id(int addr_id) 
	{
		this.addr_id = addr_id;
	}
	
	public String getCity() 
	{
		return city;
	}
	
	public void setCity(String city) 
	{
		this.city = city;
	}
	  
	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Address(int addr_id, String city, Employee employee) {
		super();
		this.addr_id = addr_id;
		this.city = city;
		this.employee = employee;
	}

	public Address() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
