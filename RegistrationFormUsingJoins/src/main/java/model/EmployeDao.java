package model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class EmployeDao 
{
	private Configuration con;
	private SessionFactory factory;
	private Session session;
	private Transaction tx;
	
	public void saveData(Employee e,Address a1) 
	{
		con = new Configuration().configure("hibernate.cfg.xml");
		factory = con.buildSessionFactory();
		session = factory.openSession();
		tx = session.beginTransaction();
		
		//Address a = session.get(Address.class,a1.getAddr_id());
		 e.setAddress(a1);
		 //e.getAddress(a1.getCity());
		//e.setAddress(a);
		 
		//session.save(a);
		session.save(e);
		
		tx.commit();
		
	}

	public boolean login(Employee emp)
	{
		boolean flag = false;
		con = new Configuration().configure("hibernate.cfg.xml");
		factory = con.buildSessionFactory();
		session = factory.openSession();
		tx = session.beginTransaction();
		List list = session.createQuery("from Employee ").list();
		Iterator it = list.iterator();
		while(it.hasNext())
		{
			Employee obj = (Employee)it.next();
			if(obj.getEmail().equals(emp.getEmail()) && obj.getPassword().equals(emp.getPassword()))
			{
				flag = true;
				
			}
		}
		return flag;
		
		
	}

	public List displayData()
	{
		con = new Configuration().configure("hibernate.cfg.xml");
		factory = con.buildSessionFactory();
		session = factory.openSession();
		tx = session.beginTransaction();
		
		List list = session.createQuery("from Employee e  join fetch e.address  ").list();
		System.out.println(list);

	
		
		return list;
	}

	public List searchData(Employee e) 
	{
		List li = new ArrayList();
		con = new Configuration().configure("hibernate.cfg.xml");
		factory = con.buildSessionFactory();
		session = factory.openSession();
		tx = session.beginTransaction();
		
		List list = session.createQuery("from Employee").list();
		Iterator it = list.iterator();
		while(it.hasNext())
		{
			Employee emp = (Employee)it.next();
			
			if(e.getEmpcode()==emp.getEmpcode() || e.getEmpname().equals(emp.getEmpname()))
			{
				
				Employee obj = new Employee();
				obj.setEmpcode(emp.getEmpcode());
				obj.setEmpname(emp.getEmpname());
				obj.setEmail(emp.getEmail());
				obj.setPassword(emp.getPassword());
				obj.setAddress(emp.getAddress());
				li.add(obj);
				
			}
			
		}
		
		return li;
	}

	public void updateData(Employee emp) 
	{
		con = new Configuration().configure("hibernate.cfg.xml");
		factory = con.buildSessionFactory();
		session = factory.openSession();
		tx = session.beginTransaction();
		
		Employee obj = session.get(Employee.class,emp.getEmpcode());
		obj.setEmpname(emp.getEmpname());
		obj.setEmail(emp.getEmail());
		obj.setPassword(emp.getPassword());
		obj.setAddress(emp.getAddress());
		session.update(obj);
		tx.commit();
	}

	public void deleteData(Employee emp) 
	{
		con = new Configuration().configure("hibernate.cfg.xml");
		factory = con.buildSessionFactory();
		session = factory.openSession();
		tx = session.beginTransaction();
		
		Employee obj = session.get(Employee.class,emp.getEmpcode());
		session.delete(obj);
		tx.commit();
	}
	
}
