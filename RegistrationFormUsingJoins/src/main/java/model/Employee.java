package model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="Employee1")
public class Employee 
{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="empcode" ,length=20)
	private int empcode;
	
	@Column(name="empname",length=20)
	private String empname;
	
	@Column(name="email",length=15)
	private String email;

	@Column(name="password" ,length=10)
	private String password;
	
	@OneToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER,orphanRemoval=true)
	@JoinColumn(name = "addr_id")
	private Address address;
	
	
	public int getEmpcode() {
		return empcode;
	}
	public void setEmpcode(int empcode) {
		this.empcode = empcode;
	}
	
	
	public String getEmpname() {
		return empname;
	}
	public void setEmpname(String empname) {
		this.empname = empname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address a) 
	{
		this.address = a;
	}
	public Employee(int empcode, String empname, String email, String password, Address address) {
		super();
		this.empcode = empcode;
		this.empname = empname;
		this.email = email;
		this.password = password;
		this.address = address;
	}
	
	
	public Employee() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getAddress(int addr_id)
	{
		// TODO Auto-generated method stub
		return addr_id;
	}
	public void setAddress(int addr_id)
	{
		// TODO Auto-generated method stub
		
	}
	
}
