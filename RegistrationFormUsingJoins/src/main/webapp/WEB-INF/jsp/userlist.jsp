<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
     <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
    <%@page isELIgnored="false" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style type="text/css">
	th {
		background : black;
		color : white;
	
	}
</style>

</head>
<body>

<center>
   <h2>List of Users</h2> <br/>
        <table border="2" width="100">
              <tr>
                <th>Employee Code</th>
                <th>Employee Name</th>
                <th>Email</th>
                <th>Password</th>
                <th>Address City</th>
            </tr>
            <c:forEach var="user" items="${data }">
                <tr>
                    <td><c:out value="${user.empcode}" /></td>
                    <td><c:out value="${user.empname}" /></td>
                    <td><c:out value="${user.email}" /></td>
                    <td><c:out value="${user.password}" /></td>
                    <td><c:out value="${user.address.city}" /></td>
                  
                </tr>
            </c:forEach>
        </table>
        
        <br/><br/>
        
        <h2> ${msg }</h2>
   

</center>





</body>
</html>