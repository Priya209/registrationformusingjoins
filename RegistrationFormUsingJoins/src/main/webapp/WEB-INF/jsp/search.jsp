<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@page isELIgnored="false" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>

<center>
   <h2>Search Employee</h2> <br/>
   	<form:form action="search" method="post" modelAttribute="bean">
   		Enter Employee Code :<form:input path="empcode"/> <br/><br/>
   	or	Enter Employee Name :<form:input path="empname"/>
   		
   		<input type="submit" value="Search"/>  <br/><br/>
   	
   	</form:form>
   	
        <table border="2" width="100">
              <tr>
                
                <th>Employee Code</th>
                <th>Employee Name</th>
                <th>Employee Email</th>
                <th>Employee Password</th>
                 <th>Employee Address</th>
                <th>Update</th>
                <th>Delete</th>
            </tr>
              <c:forEach var="tab" items="${li }">
                <tr>
                    <td> <form action="update" method="post" modelAttribute="bean" >
                    <input type="text" value=" ${tab.empcode}" name="empcode" readonly> </td>
                    <td><input type="text" value="${tab.empname}" name="empname" ></td>
                    <td><input type="text" value="${tab.email}" name="email"></td>
                    <td><input type="text" value="${tab.password}" name="password" ></td>
                    <td><input type="text" value="${tab.address.city}" name="address.city" ></td>
                    <td align="center"><input type="submit" value="Update"></form></td>
                	
                	<td> <form method="post" action="delete" modelAttribute="bean">
                		<input type="hidden" value="${tab.empcode }" name="empcode">
                		<input type="submit" value="Delete">
                		</form></td>
                
                
                </tr>
            </c:forEach>
        </table>
        
        <br/><br/>
        
        <h2> ${msg }</h2>
   

</center>





</body>
</html>